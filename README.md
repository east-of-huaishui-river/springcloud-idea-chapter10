# springcloud-idea-chapter10

#### 介绍
Spring cloud 实战电商网站项目

目的： 综合应用 spring cloud进行微服务架构开发。

#### 开发环境

操作系统 ： windows

Java环境 ： JDK8

开发工具 ： Idea 2017

数据库： mysql
 
spring cloud : Greenwich.SR2

spring boot : 2.1.7 Release

